import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import image from '../../assets/wool.png';


class Header extends React.Component{
    render() {
        return(
            <View style={styles.content}>
                <Image style={styles.image} source={image}/>
                <Text style={styles.title}>{this.props.title}</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    content: {
        alignItems: 'center',
        margin: 10,
    },
    image: {
        resizeMode: 'contain',
        height: 100,
    },
    title: {
        textAlign: 'center',
        color: 'white',
        fontSize: 35,
        fontWeight: 'bold',
    },
});

export default Header;
